#!/bin/bash

set -e

cat >>~/.bashrc <<"EOF"
# DEBEMAIL="marko@dimjasevic.net"
# DEBFULLNAME="Marko Dimjašević"
# export DEBEMAIL DEBFULLNAME
alias ll='ls -l'
alias la='ls -al'
EOF

# Set up quilt
cat >>~/.quiltrc <<"EOF"
d=. ; while [ ! -d $d/debian -a `readlink -e $d` != / ]; do d=$d/..; done
if [ -d $d/debian ] && [ -z $QUILT_PATCHES ]; then
    # if in Debian packaging tree with unset $QUILT_PATCHES
    QUILT_PATCHES="debian/patches"
    QUILT_PATCH_OPTS="--reject-format=unified"
    QUILT_DIFF_ARGS="-p ab --no-timestamps --no-index --color=auto"
    QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
    QUILT_COLORS="diff_hdr=1;32:diff_add=1;34:diff_rem=1;31:diff_hunk=1;33:diff_ctx=35:diff_cctx=33"
    if ! [ -d $d/debian/patches ]; then mkdir $d/debian/patches; fi
fi
EOF

source ~/.bashrc

# Install Debian developer tools
sudo apt-get install --yes build-essential debhelper dh-make fakeroot gnupg lintian patch patchutils quilt pbuilder emacs

# Optional Debian developer packages
# sudo apt-get install --yes autoconf automake autotools-dev devscripts xutils-dev

# Add jessie backports
sudo bash -c "cat >>/etc/apt/sources.list" <<EOF
# Backports
deb http://mirrors.kernel.org/debian jessie-backports main
deb-src http://mirrors.kernel.org/debian jessie-backports main
EOF

sudo apt-get update

# Installs dependencies
dependencies="openjdk-8-jdk mercurial ant junit4 jarwrapper"
sudo apt-get install --yes --target-release jessie-backports $dependencies

package=jpf
upstream_commit=29
upstream_version=${upstream_commit}
version="${upstream_version}"
package_version=${package}-${version}
# package revision
package_revision="1"
package_dir=${package_version}
package_dir_absolute=~/${package_version}
archive_ext=tar.gz
archive_name="${package_version}.${archive_ext}"

# Upstream values
upstream_name=jpf
upstream_archive=${upstream_name}-${upstream_version}.tar.gz
upstream_dir=${upstream_name}-${upstream_version}

# Clone the JPF repo and checkout a specific commit known to work
cd
hg clone http://babelfish.arc.nasa.gov/hg/jpf/jpf-core ${package_dir}
cd ${package_dir}
hg update ${upstream_commit}

# Tell JPF where JUnit is
echo "env.JUNIT_HOME=/usr/share/java" > ${package_dir_absolute}/local.properties

# Start packaging
export DEBEMAIL="marko@dimjasevic.net"
export DEBFULLNAME="Marko Dimjašević"
# Important: don't tar by providing an absolute path to the target!
cd
tar czf ${archive_name} --exclude-vcs ${package_dir}

cd ${package_dir_absolute}
dh_make --indep --yes --copyright apache --file ../${archive_name}

# Update debian/control
cp /vagrant/debian/control ${package_dir_absolute}/debian/

# Update debian/rules
cp /vagrant/debian/rules ${package_dir_absolute}/debian/

# Update copyright
cp /vagrant/debian/copyright ${package_dir_absolute}/debian/

java_home=/usr/lib/jvm/java-8-openjdk-$(dpkg-architecture -qDEB_HOST_ARCH)
sudo update-alternatives --set java  ${java_home}/jre/bin/java
sudo update-alternatives --set javac ${java_home}/bin/javac

# Apply a patch that fixes a path in bin/jpf
cd ${package_dir_absolute}
mkdir -p debian/patches

patch_p1=fix-paths.patch
# quilt import /vagrant/debian/patches/${patch_p1}
# quilt push -a
